package com.example.t15sep24;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import com.example.t15sep24.my_interface.KeyInterface;
import com.example.t15sep24.view.parts.MySideViewClass;

import android.app.Activity;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import library.widget.DrawerLayoutUpperActivityClass;
import library.widget.PreferenceClass;

public class XpathActivity extends DrawerLayoutUpperActivityClass implements KeyInterface{
	
	private String mUrl = "";
	private String mSrc = "";
	
	private Context context;
	private Activity activity;

	private PreferenceClass pref;

	private EditText queryEdit;

	private TextView textView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getApplicationContext();
		activity = this;
		mainView();
		setSideView(new MySideViewClass(activity), LayoutParams.MATCH_PARENT);
		
		
		pref = new PreferenceClass(context, KEY_FILE);
		
		mSrc = pref.loadString(SRC);
		
	};
/**
 * ここにXpathの検査のViewを実装してください。
 * 
 */
	private void mainView() {
ScrollView scrollView = new ScrollView(this);
    	
    	LinearLayout linearLayout = new  LinearLayout(this);
    	linearLayout.setOrientation(LinearLayout.VERTICAL);  
    	scrollView.addView(linearLayout);
    	
    	queryEdit = new EditText(this);
    	linearLayout.addView(queryEdit);
    	
    	Button getSrcButton = new Button(this);
    	
    	linearLayout.addView(getSrcButton);
    	getSrcButton.setText("getSrc");
		getSrcButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				

				String result = getData(mSrc, queryEdit.getText().toString());
				textView.setText(result);
				Toast.makeText(activity, "" + queryEdit.getText().toString(), Toast.LENGTH_SHORT).show();
			}
		});
		
		textView = new TextView(this);
    	textView.setText("no src");
    	linearLayout.addView(textView);
    	
    	
    	setMainView(scrollView);
		
	}
	
	private String getTitle(String src){
    	String result = "";
    	
    	try{
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbFactory.newDocumentBuilder();
            Document document = builder.parse(new ByteArrayInputStream(src.getBytes()));
        
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();
            
            result = xpath.evaluate("/html/head/title/text()",document);
    		
    		
    	}catch(Exception exception){
    		Toast.makeText(activity, exception.getClass().getName() +": " + exception.getMessage(), Toast.LENGTH_SHORT).show();
    		result = exception.getClass().getName() +": " + exception.getMessage();
    	}
    	
    	return result; 
    }
	/**
	 * こっちには　検査時の処理を
	 * 
	 */
	private String getData(String src, String query){
		String result = "";
    	
    	try{
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbFactory.newDocumentBuilder();
            Document document = builder.parse(new ByteArrayInputStream(src.getBytes()));
        
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();
            
            result = xpath.evaluate(query,document);
    		
    		
    	}catch(Exception exception){
    		Toast.makeText(activity, exception.getClass().getName() +": " + exception.getMessage(), Toast.LENGTH_SHORT).show();
    		exception.printStackTrace();
    		result = exception.getClass().getName() +": " + exception.getMessage();
    		
    	}
    	
    	return result; 
	}
	
}
