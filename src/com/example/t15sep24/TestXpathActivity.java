package com.example.t15sep24;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import com.example.t15sep24.my_interface.KeyInterface;
import com.example.t15sep24.view.parts.MySideViewClass;

import android.app.Activity;
import android.app.ActionBar.LayoutParams;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import largetext.LargeTextClass;
import library.widget.DrawerLayoutUpperActivityClass;
import library.widget.PreferenceClass;
import library.widget.XpathEscapeClass;

public class TestXpathActivity extends DrawerLayoutUpperActivityClass implements KeyInterface{
	
	private String mUrl = "";
	private String mSrc = "";
	
	private Context context;
	private Activity activity;

	private PreferenceClass pref;

	private EditText queryEdit;
	private EditText sourceDataEdit;

	private TextView resultText;
	private TextView escapedText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getApplicationContext();
		activity = this;
		mainView();
		setSideView(new MySideViewClass(activity), LayoutParams.MATCH_PARENT);
		
		
		pref = new PreferenceClass(context, KEY_FILE);
		
		mSrc = pref.loadString(SRC);
		
	};
/**
 * ここにXpathの検査のViewを実装してください。
 * 
 */
	private void mainView() {
ScrollView scrollView = new ScrollView(this);
    	
    	LinearLayout linearLayout = new  LinearLayout(this);
    	linearLayout.setOrientation(LinearLayout.VERTICAL);  
    	scrollView.addView(linearLayout);
    	
    	resultText = new TextView(this);
    	resultText.setText("no src");
    	linearLayout.addView(resultText);
    	
    	Button getSrcButton = new Button(this);
    	
    	linearLayout.addView(getSrcButton);
    	getSrcButton.setText("getSrc");
		getSrcButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				String src = mSrc;
				String src = sourceDataEdit.getText().toString();
				String result = getData(src, queryEdit.getText().toString());
				resultText.setText("result: " + result);
				Toast.makeText(activity, "" + queryEdit.getText().toString(), Toast.LENGTH_SHORT).show();
			}
		});
		
    	queryEdit = new EditText(this);
    	linearLayout.addView(queryEdit);
    	queryEdit.setHint("query on Xpath");
    	queryEdit.setText("/html/head/title/text()");
		
    	sourceDataEdit = new EditText(this);
    	linearLayout.addView(sourceDataEdit);
    	sourceDataEdit.setHint("Xml source code");
    	sourceDataEdit.setText(LargeTextClass.str);
    	sourceDataEdit.setTextSize(15);
    	
		
    	
    	escapedText = new TextView(this);
    	escapedText.setText(XpathEscapeClass.escape(LargeTextClass.str));
    	linearLayout.addView(escapedText);
    	
    	Button toClipbutton = new Button(this);
    	linearLayout.addView(toClipbutton);
    	toClipbutton.setText("to Clip board");
    	toClipbutton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//クリップボードに格納するItemを作成
				ClipData.Item item = new ClipData.Item(escapedText.getText().toString());
				 
				//MIMETYPEの作成
				String[] mimeType = new String[1];
				mimeType[0] = ClipDescription.MIMETYPE_TEXT_URILIST;
				 
				//クリップボードに格納するClipDataオブジェクトの作成
				ClipData cd = new ClipData(new ClipDescription("text_data", mimeType), item);
				 
				//クリップボードにデータを格納
				ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
				cm.setPrimaryClip(cd);
			}
		});
    	setMainView(scrollView);
		
	}
	
	/**
	 * こっちには　検査時の処理を
	 * 
	 */
	private String getData(String src, String query){
		String result = "";
		src = XpathEscapeClass.escape(src);
    	try{
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbFactory.newDocumentBuilder();
            Document document = builder.parse(new ByteArrayInputStream(src.getBytes()));
        
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();
            
            result = xpath.evaluate(query,document);
    		
    		
    	}catch(Exception exception){
    		Toast.makeText(activity, exception.getClass().getName() +": " + exception.getMessage(), Toast.LENGTH_SHORT).show();
    		exception.printStackTrace();
    		result = exception.getClass().getName() +": " + exception.getMessage();
    		
    	}
    	
    	return result; 
	}
	
	
	
	
	
}
