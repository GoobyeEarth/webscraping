package com.example.t15sep24;

import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;

import java.text.Collator;

import com.example.t15sep24.my_interface.KeyInterface;
import com.example.t15sep24.view.parts.MySideViewClass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceCategory;
import android.support.v4.view.MenuItemCompat;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.SearchView.OnQueryTextListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.Toast;
import library.my_interface.SetIntStringArrayInterface;
import library.widget.DataPreference;
import library.widget.DrawerLayoutUpperActivityClass;
import library.widget.LibAndroidClass;
import library.widget.PreferenceClass;
import library.widget.SimpleListViewClass;
//
public class MainActivity extends DrawerLayoutUpperActivityClass implements KeyInterface{
	private SearchView searchView;
	
	private String mUrl = "";
	private String mSrc = "";
	
	private Context context;
	private Activity activity;

	private WebView webView;
	private PreferenceClass pref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getApplicationContext();
		activity = this;
		
		mainView();
		setSideView(new MySideViewClass(activity), LayoutParams.MATCH_PARENT);
		
		pref = new PreferenceClass(context, KEY_FILE);
		
	}
	
	private void mainView(){
		webView = new WebView(context);
		
		webView.loadUrl("http://jp.reuters.com/investing/currencies");

		webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        
        class SourceCodeFromView{
        	@JavascriptInterface
            public void getSrc(final String src) {
        		mSrc = src;
                pref.saveString(SRC, src);
                Toast.makeText(context, "finished", Toast.LENGTH_SHORT).show();
            }
        }
        webView.addJavascriptInterface(new SourceCodeFromView(), "srcgetter");
        
        webView.setWebViewClient(new WebViewClient(){
        	@Override
    		public void onPageFinished(WebView view, String url) {
    			
    			view.loadUrl("javascript:window.srcgetter.getSrc(document.documentElement.outerHTML);");
    		}

    		@Override
    		public boolean shouldOverrideUrlLoading(WebView view, String url) {
    			getActionBar().setTitle(url);
    			mUrl = url;
    			return super.shouldOverrideUrlLoading(view, url);
    		}
    		
    		
        });
        
        
        
        
		setMainView(webView);
	}
	
	@JavascriptInterface
    public void viewSource(final String src) {
		mSrc = src;
        pref.saveString(SRC, src);
    }
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // アクションバー内に使用するメニューアイテムを注入します。
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_activity_actions, menu);
	    
	    View homeIcon = findViewById(android.R.id.home);
	    homeIcon.setVisibility(View.GONE);
	    
//	    getActionBar().hide();
//	    getActionBar().show();
//	    
	    setSearchView(menu);
	    
	    
	    return super.onCreateOptionsMenu(menu);
	}
	
	
	private void setSearchView(Menu menu){
		MenuItem menuItem =  menu.findItem(R.id.searchView);
	    searchView = (SearchView) menuItem.getActionView();
	    
	    searchView.setIconifiedByDefault(true);
        
        searchView.setSubmitButtonEnabled(true);
	    searchView.setOnQueryTextListener(new OnQueryTextListener() {
			
			@Override
			public boolean onQueryTextSubmit(String query) {
//				accessToUrl(query);
				searchPressed(query);
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				// TODO Auto-generated method stub
				return false;
			}
		});
	    
	    
	}
	
	private void accessToUrl(String query){
		Toast.makeText(this, "go to " + query, Toast.LENGTH_SHORT).show();
	}
	
	private boolean searchPressed(String query) {
        ActionBar actionBar = getActionBar();
        actionBar.setTitle(query);
        actionBar.setDisplayShowTitleEnabled(true);
        if (query != null && !query.equals("")) {
           accessToUrl(query);
        }
        // 虫眼鏡アイコンを隠す
        this.searchView.setIconified(false);
        // SearchViewを隠す
        this.searchView.onActionViewCollapsed();
        // Focusを外す
        this.searchView.clearFocus();
        return false;
    }
	
}
