package com.example.t15sep24;

import android.app.Activity;

import com.example.t15sep24.my_interface.KeyInterface;
import com.example.t15sep24.view.parts.MySideViewClass;

import android.app.ActionBar.LayoutParams;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import library.widget.DrawerLayoutUpperActivityClass;
import library.widget.PreferenceClass;

public class SourceCodeActivity extends DrawerLayoutUpperActivityClass implements KeyInterface{
	private Context context;
	private Activity activity;
	private PreferenceClass pref;
	
	private String mSrc;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getApplicationContext();
		activity = this;
		pref = new PreferenceClass(context, KEY_FILE);
		mSrc = pref.loadString(SRC);
		mainView();
//		textView();
//		textScrollView();
		
		setSideView(new MySideViewClass(activity), LayoutParams.MATCH_PARENT);
	}
	
	private void textScrollView(){
		ScrollView scrollView = new ScrollView(this);
		setMainView(scrollView);
		
		TextView textView = new TextView(this);
		textView.setText(mSrc);
		scrollView.addView(textView);
		
	}
	
	private void mainView(){
		
		ScrollView scrollView = new ScrollView(this);
		setMainView(scrollView);
		
		LinearLayout linearLayout = new LinearLayout(this);
		scrollView.addView(linearLayout);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		
		Button toClipboardButton = new Button(activity);
		linearLayout.addView(toClipboardButton);
		toClipboardButton.setText("to clip board");
		toClipboardButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//クリップボードに格納するItemを作成
				ClipData.Item item = new ClipData.Item(mSrc);
				 
				//MIMETYPEの作成
				String[] mimeType = new String[1];
				mimeType[0] = ClipDescription.MIMETYPE_TEXT_URILIST;
				 
				//クリップボードに格納するClipDataオブジェクトの作成
				ClipData cd = new ClipData(new ClipDescription("text_data", mimeType), item);
				 
				//クリップボードにデータを格納
				ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
				cm.setPrimaryClip(cd);
			}
		});

		TextView textView = new TextView(this);
		textView.setText(mSrc);
		linearLayout.addView(textView);
		
		
	}
	
	
}
