package com.example.t15sep24.view.parts;

import java.util.Set;

import com.example.t15sep24.*;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import library.my_interface.SetIntStringArrayInterface;
import library.widget.LibAndroidClass;
import library.widget.SimpleListViewClass;

public class MySideViewClass extends SimpleListViewClass{

	public MySideViewClass(final Activity activity) {
		super(activity);
		setBackgroundColor(Color.WHITE);
		setChildView(null);
		add(new String[]{"サイトを見る"}, new SetIntStringArrayInterface() {
			@Override
			public void setProcess(int num, String[] str) {
				LibAndroidClass.callActivity(activity, MainActivity.class);
			}
		});

		add(new String[]{"ソースを見る"}, new SetIntStringArrayInterface() {
			
			@Override
			public void setProcess(int num, String[] textArray) {
				
				LibAndroidClass.callActivity(activity, SourceCodeActivity.class);
				
				
			}
		});
		
		add(new String[]{"ブックマーク"}, null);
		
		add(new String[]{"Xpathで検索する"},new SetIntStringArrayInterface() {
			
			@Override
			public void setProcess(int num, String[] textArray) {
				LibAndroidClass.callActivity(activity, XpathActivity.class);
				
			}
		});
		
		add(new String[]{"Xpathのテスト"}, new SetIntStringArrayInterface() {
			
			@Override
			public void setProcess(int num, String[] textArray) {
				LibAndroidClass.callActivity(activity, TestXpathActivity.class);
				
			}
		});
		
		setData();
		
		setOnItemClickListener(null);
	}
	

}
